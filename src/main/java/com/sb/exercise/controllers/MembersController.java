package com.sb.exercise.controllers;

import com.sb.exercise.repositories.FinderOldMan;
import com.sb.exercise.repositories.MembersGroupRepository;
import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@Validated
@RestController
public class MembersController {

    private final MembersGroupRepository repository;

    public MembersController(MembersGroupRepository repository) {
        this.repository = repository;
    }

    @RequestMapping("/members")
    public Set<String> findOldMembers(@Range(min = 0, max = 150, message = "Please enter realistic age")
                                      @RequestParam(value = "minAge", defaultValue = "0") int minAge) {
        FinderOldMan finder = new FinderOldMan(minAge);
        return repository.find(finder);
    }

}
