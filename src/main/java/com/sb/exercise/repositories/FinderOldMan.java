package com.sb.exercise.repositories;

import com.sb.exercise.models.Member;
import com.sb.exercise.models.MembersGroup;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FinderOldMan implements MembersFinder<Set<String>> {

    private final int minAge;

    public FinderOldMan(int minAge) {
        this.minAge = minAge;
    }

    public Set<String> find(List<MembersGroup> groups) {
        return groups.stream()
                .flatMap(g -> g.getMembers().stream())
                .filter(m -> m.getAge() > minAge)
                .map(Member::getName)
                .collect(Collectors.toSet());
    }

}
