package com.sb.exercise.repositories;

import com.sb.exercise.models.Member;
import com.sb.exercise.models.MembersGroup;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Repository
@Scope("singleton")
public class MembersGroupRepository {

    private List<MembersGroup> groups = new LinkedList<>();

    public MembersGroupRepository() { // hardcode
        MembersGroup group1 = new MembersGroup("Group 1", Arrays.asList(
                new Member("Name 1", 20),
                new Member("Name 2", 30),
                new Member("Name 3", 40)
        ));

        MembersGroup group2 = new MembersGroup("Group 2", Arrays.asList(
                new Member("Name 3", 40),
                new Member("Name 4", 50),
                new Member("Name 5", 60)
        ));

        this.save(group1);
        this.save(group2);
    }

    public void save(MembersGroup group) {
        this.groups.add(group);
    }

    public List<MembersGroup> getAll() {
        return this.groups;
    }

    public <T> T find(MembersFinder<T> finder) {
        List<MembersGroup> groups = this.getAll();
        return finder.find(groups);
    }
}
