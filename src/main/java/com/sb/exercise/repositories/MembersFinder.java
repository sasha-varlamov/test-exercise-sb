package com.sb.exercise.repositories;

import com.sb.exercise.models.MembersGroup;

import java.util.List;

public interface MembersFinder<T> {

    T find(List<MembersGroup> listMembers);

}
