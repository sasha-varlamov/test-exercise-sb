package com.sb.exercise;

import com.sb.exercise.models.Member;
import com.sb.exercise.models.MembersGroup;
import com.sb.exercise.repositories.FinderOldMan;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class FinderOldManTest {

    private static FinderOldMan finder;
    private static MembersGroup group20_40;
    private static MembersGroup group40_60;
    private static List<MembersGroup> groups;


    @BeforeClass
    public static void setUp() {
        group20_40 = new MembersGroup("Group 1", Arrays.asList(
                new Member("Name 20", 20),
                new Member("Name 30", 30),
                new Member("Name 40", 40)
        ));

        group40_60 = new MembersGroup("Group 2", Arrays.asList(
                new Member("Name 40", 40),
                new Member("Name 50", 50),
                new Member("Name 60", 60)
        ));

        groups = Arrays.asList(group20_40, group40_60);
        finder = new FinderOldMan(30);
    }


    @Test
    public void shouldFindInEmptyList() {
        Set<String> result = finder.find(Collections.emptyList());
        assertTrue(result.isEmpty());
    }

    @Test
    public void shouldFindInEmptyGroup() {
        MembersGroup emptyGroup = new MembersGroup("Empty group", Collections.emptyList());
        Set<String> result = finder.find(Collections.singletonList(emptyGroup));
        assertTrue(result.isEmpty());
    }

    @Test
    public void shouldReturnOldManGreaterThan() {
        Set<String> result = finder.find(Collections.singletonList(group20_40));
        assertThat(result, containsInAnyOrder("Name 40"));
        assertThat(result.size(), is(1));
    }

    @Test
    public void shouldReturnOldManFromDifferentGroups() {
        Set<String> result = finder.find(groups);
        assertThat(result, containsInAnyOrder("Name 40", "Name 50", "Name 60"));
        assertThat(result.size(), is(3));
    }

}
